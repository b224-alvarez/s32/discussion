let http = require("http");

//mock database
let directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com",
  },
  {
    name: "Jobert",
    email: "jobert@mail.com",
  },
];

// GET METHOD
http
  .createServer(function (request, response) {
    // Route for returning all items upon receiving a GET request
    if (request.url == "/users" && request.method == "GET") {
      // Requests the "/items" path and "GETS" information
      // Sets the status code to 200, denoting OK
      // Sets response output to JSON data type
      response.writeHead(200, { "Content-Type": "application/json" });
      //  JSON.stringify() - method convertes the string input to JSON
      response.write(JSON.stringify(directory));
      response.end();
    }

    // POST METHOD
    if (request.url == "/addUser" && request.method == "POST") {
      // Declare and intialize a "requestBody" variable to an empty string
      let requestBody = "";

      // Data is received from the client and is processed in the "data" stream
      request.on("data", function (data) {
        // Assigns the data retrieved from the data stream to requestBody
        requestBody += data;
      });

      // response end step - only runs after the request has completely been sent
      request.on("end", function () {
        // Check if at this point the requestBody is of data type STRING
        console.log(typeof requestBody);
        // Converts the string requestBody to JSON
        requestBody = JSON.parse(requestBody);

        // Create a new object representing the new mock database record
        let newUser = {
          name: requestBody.name,
          email: requestBody.email,
        };

        // Add the new user into the mock database
        directory.push(newUser);
        console.log(directory);

        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(JSON.stringify(newUser));
        response.end();
      });
    }
  })
  .listen(3000);

console.log("Server running at localhost:3000");
